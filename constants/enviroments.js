const config = {
  local: {
    port: 9000,
    mongoURI: 'mongodb+srv://paolaMartinez:123@clusterfree.igzif.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    privateKey: 'secret',
    parkingURI: 'https://dev.parcoapp.com/api/Parkings',
  },
};

module.exports = config['local'];
