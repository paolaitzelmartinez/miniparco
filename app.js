const express = require('express');
const app = express();
const { port } = require('./constants/enviroments');

const authRoutes = require('./routes/public');
const privateRoutes = require('./routes/private');

app.use(express.json());

app.use('/auth', authRoutes);
app.use('/private', privateRoutes);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
