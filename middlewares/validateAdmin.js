const { validateAdminToken } = require('./validateToken');

module.exports = (schema) => {
  return async (req, res, next) => {
    try {
      const paramasCorrect = await schema.validateAsync(req.body);
      paramasCorrect
        ? validateAdminToken(req, res, next)
        : res.send(error);
    } catch (error) {
      res.send(error);
    }
  };
};