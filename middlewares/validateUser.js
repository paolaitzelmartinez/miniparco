const { validateUserToken } = require('./validateToken');

module.exports = (schema ) => {
  return async (req, res, next) => {
    try {
      const paramasCorrect = await schema.validateAsync(req.body);
      paramasCorrect
        ? validateUserToken(req, res, next)
        : res.send(error);
    } catch (error) {
      res.send(error);
    }
  };
};