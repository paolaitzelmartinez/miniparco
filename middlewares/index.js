const validateData = require('./validateData');
const validateUser = require('./validateUser');
const validateAdmin = require('./validateAdmin');
const { validateUserToken, validateAdminToken } = require('./validateToken');

module.exports = {
  validateData,
  validateUser,
  validateAdmin,
  validateUserToken,
  validateAdminToken
};
