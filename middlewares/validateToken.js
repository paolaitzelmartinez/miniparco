const userModel = require('../src/models/user');

const validateUserToken = (req, res, next) => {
  const access_token = req.query.access_token;
  if (!access_token) {
    res.status(401).json({ message: 'Unauthorized' });
  } else {    
    userModel.findOne({ _id: req.body.id }, function (err, user) {
      if (err) {
        res.send(err);
      } else {
        if (user) {
          if (user.accessToken === access_token) {
            next();
          } else {
            res.status(401).json({ message: 'Unauthorized' });
          }
        } else {
          res.status(401).json({ message: 'Unauthorized' });
        }
      }
    });
  }
};

const validateAdminToken = (req, res, next) => {
  const access_token = req.query.access_token;
  if (!access_token) {
    res.json({ code: 401, message: 'Unauthorized' });
  } else {    
    userModel.findOne({ _id: req.body.id, isAdmin: true }, function (err, user) {
      if (err) {
        res.send(err);
      } else {
        if (user) {
          if (user.accessToken === access_token) {
            next();
          } else {
            res.json({ code: 401, message: 'Unauthorized' });
          }
        } else {
          res.json({ code: 404, message: 'Not found' });
        }
      }
    });
  }
};

module.exports = { validateUserToken, validateAdminToken };
