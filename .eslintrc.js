module.exports = {
  env: {
    browser: true,
    node: true,
    es2021: true,
  },
  parserOptions: {
    ecmaFeatures: { jsx: true },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['prettier'],
  rules: {
    'linebreak-style': 0,
    'arrow-body-style': 0,
    'prefer-arrow-callback': 0,
    'no-unused-expressions': [2, { allowShortCircuit: true, allowTernary: true }],
    complexity: [2, { max: 10 }],
    'comma-dangle': [2, 'always-multiline'],
    'prettier/prettier': 2,
    'object-curly-newline': ['error', { consistent: true }],
  },
  settings: {
    'import/resolver': {
      node: {
        paths: ['.'],
        extensions: ['.js', '.jsx'],
      },
    },
  },
};
