const { connect, disconnect }= require('./lib');

(async () => {
  await connect();
  require('./app.js');
})();

process.on('SIGINT', () => {
  disconnect().then((state) => {
    console.log(state);
    process.exit(0);
  });
});