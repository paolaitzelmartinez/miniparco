const express = require('express');
const router = express.Router();
const { updateUser } = require('../src/controllers/user');
const { getAllParkings } = require('../src/controllers/parkings');
const { payParking, getUserTransactions } = require('../src/controllers/transactions');
const { getReport } = require('../src/controllers/report');
const { validateUserToken, validateUser, validateAdmin } = require('../middlewares');
const {
  updateUserSchema,
  paySchema,
  getUserTransactionSchema,
  reportSchema
} = require('../src/schemas');

router.patch('/update', validateUser(updateUserSchema), updateUser);
router.get('/parkings', validateUserToken, getAllParkings);
router.post('/pay', validateUser(paySchema), payParking);
router.get('/my-transactions', validateUser(getUserTransactionSchema), getUserTransactions);
router.get('/report', validateAdmin(reportSchema), getReport);

module.exports = router;