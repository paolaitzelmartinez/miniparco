const express = require('express');
const router = express.Router();
const { createUser } = require('../src/controllers/user');
const { login } = require('../src/controllers/login');
const { createUserSchema, loginSchema } = require('../src/schemas');
const { validateData } = require('../middlewares');

router.post('/signup', validateData(createUserSchema), createUser);
router.post('/login', validateData(loginSchema), login);

module.exports = router;