const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema({
  _id: { type: Schema.Types.ObjectId, auto: true },
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  cellphone: { type: Number, required: true },
  balance: { type: Number, default: 0 },
  accessToken: { type: String },
  isAdmin: { type: Boolean, default: 0 },
  createdAt: { type: Date, default: Date.now },
  transactions: [{ type: Schema.Types.ObjectId, ref: 'transaction' }],
});

const model = mongoose.model('user', schema);

module.exports = model;