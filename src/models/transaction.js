const mongoose = require('mongoose');
const { Schema } = mongoose;

const schema = new Schema({
  _id: { type: Schema.Types.ObjectId, auto: true },
  amount: { type: Number, required: true },
  ticket: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
});

const model = mongoose.model('transaction', schema);

module.exports = model;