const transactionModel = require('../models/transaction');
const ObjectsToCsv = require('objects-to-csv');
const fs = require('fs');

const createCSV = (data) => {
  return async (res) => {
    try {
      const csv = new ObjectsToCsv(data.map(item => ({
        id: item.id,
        amount: item.amount,
        ticket: item.ticket,
        createdAt: item.createdAt,
      })));

      await csv.toDisk('./report.csv');

      return res.download('./report.csv', () => {
        fs.unlinkSync('./report.csv');
      });
    } catch (error) {
      res.send(error);
    }
  };
};
  
module.exports = {
  getReport: (_, res) => {
    const { dateStart, dateEnd, parkingId } = req.body;
    const rules = {
      parkingId,
      createdAt: {
        $gte: dateStart,
        $lte: dateEnd
      }
    };

    transactionModel.find(rules, (err, data) => {
      if (err) {
        res.status(500).json({ message: 'Server error', err });
      } else {
        createCSV(data)(res);
      }
    });
  }
};
