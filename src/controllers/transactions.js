const transactionModel = require('../models/transaction');
const userModel = require('../models/user');
const { parkingURI } = require('../../constants/enviroments');
const axios = require("axios");

const validateParking = (data, parkingId) => (
  data.find(parking => parking.status === 0 && parking.id === parkingId)
);

const validateBalance = (id, amount, parkingId, res) => (
  userModel.findOne({ _id: id, balance: {$gte:amount} }, function (err, user) {
    if (err) {
      res.status(500).json({ message: 'Server error' });
    } else {
      if (user) {
        const newTransaction = new transactionModel({ id, amount, ticket: 1, parkingId });

        newTransaction.save(function (err, user) {
          err
            ? res.status(500).json({ message: 'Server error' })
            : res.status(500).json({ message: 'Transaction created successfully', data: user });
        });
      } else {
        res.status(404).json({ code: 404, message: "User or Balance error" });
      }
    }
  })
);
  
module.exports = {
  getUserTransactions: (req, res) => {
    const { id } = req.body;
    transactionModel.find({ id }, function (err, transactions) {
      err
        ? res.status(500).json({ message: 'Server error, Find transactions' })
        : res.status(200).json({ message: 'Transactions found successfully', data: transactions });
    });
  },
  payParking: function (req, res) {
    axios({
      url: parkingURI,
      method: "get",
    })
    .then(response => {
      const { id, amount, parkingId } = req.body;
      const parking = validateParking(response.data, parkingId);
      
      parking
        ? validateBalance(id, amount, parkingId, res)
        : res.status(404).json({ message: "Parking not allowed" });
    })
    .catch((err) => res.status(500).json(err) );
  },
};
