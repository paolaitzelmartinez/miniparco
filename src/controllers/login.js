const userModel = require('../models/user');
const { privateKey } = require('../../constants/enviroments');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
  
module.exports = {
  login: function (req, res) {
    const { email, password } = req.body;

    userModel.findOne({ email }, (err, user) => {
      if (err) {
        res.status(500).json({ message: 'Server error' });
      } else {
        if (!user) {
          res.status(401).json({ message: 'Not Found' });
        } else {
          bcrypt.compare(password, user.password, (err, check) => {
            if (err) {
              res.status(500).json({ message: 'Server error' });
            } else {
              if (!check) {
                res.status(401).json({ message: 'Unauthorized' });
              } else {
                jwt.sign({ name: user.name, email: user.email }, privateKey, function (err, accessToken) {
                  if (err) {
                    res.status(500).json({ message: 'Server error' });
                  } else {
                    userModel.updateOne({ _id: user.id }, { accessToken }, (err) => {
                      err
                        ? res.status(500).json({ message: 'Server error' })
                        : res.status(200).json({ message: 'Login successfully', accessToken });
                    });
                  }
                });
              }
            }
          });
        }
      }
    });
  },
};
