const { parkingURI } = require('../../constants/enviroments');
const axios = require("axios");
  
module.exports = {
  getAllParkings: function (_, res) {
    axios({
      url: parkingURI,
      method: "get",
    })
    .then(response => res.status(200).json(response.data) )
    .catch(() => res.status(500).json({ message: 'Server error' }) );
  },
};
