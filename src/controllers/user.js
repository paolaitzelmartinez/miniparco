const userModel = require('../models/user');
const passwordEncrypt = require('../../utils/passwordEncrypt');
  
module.exports = {
  createUser: (req, res) => {
    const { name, email, password, cellphone } = req.body;
    const newUser = new userModel({
      name,
      email,
      cellphone,
      password: passwordEncrypt(password)
    });

    newUser.save(function (err, user) {
      err
        ? res.status(500).json({ message: 'Server error' })
        : res.status(200).json({ message: 'User created successfully', data: user });
    });
  },
  updateUser: function (req, res) {
    const { id, name, email, password, cellphone, balance } = req.body;

    userModel.findOne({ _id: id }, function (err, user) {
      if (err) {
        res.send(err);
      } else {
        if (user) {
          let update = { name, email, password, cellphone, balance };

          if (password) update.password = passwordEncrypt(password);

          userModel.findOneAndUpdate({ _id: id }, update, { new: true }, (err, user) => {
            err
              ? res.status(500).json({ message: 'Server error' })
              : res.status(200).json({ message: 'Update successfully', data: user });
          });
        } else {
          res.status(404).json({ message: 'User not found' });
        }
      }
    });
  },
};
