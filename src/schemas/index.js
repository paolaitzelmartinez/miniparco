
const loginSchema = require('./login');
const reportSchema = require('./report');
const { createUserSchema, updateUserSchema } = require('./user');
const { paySchema, getUserTransactionSchema } = require('./transaction');

module.exports = {
  createUserSchema,
  updateUserSchema,
  loginSchema,
  paySchema,
  getUserTransactionSchema,
  reportSchema
};