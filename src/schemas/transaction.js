const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);

const paySchema = Joi.object().keys({
  id: Joi.objectId().required(),
  parkingId: Joi.string().required(),
  amount: Joi.number().positive().required(),
});

const getUserTransactionSchema = Joi.object().keys({
  id: Joi.objectId().required(),
});

module.exports = { paySchema, getUserTransactionSchema };
