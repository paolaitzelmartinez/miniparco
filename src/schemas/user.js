const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);

const createUserSchema = Joi.object().keys({
  id: Joi.objectId(),
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  cellphone: Joi.number().integer().positive().required(),
  balance: Joi.number().integer().positive().default(0),
  isAdmin: Joi.boolean().default(false),
  created_at: Joi.date().default(new Date()),
  transaction_id: Joi.string(),
});

const updateUserSchema = Joi.object().keys({
  id:Joi.objectId().required(),
  name: Joi.string(),
  email: Joi.string().email(),
  password: Joi.string(),
  cellphone: Joi.number().integer().positive(),
  balance: Joi.number().integer().positive(),
});

module.exports = { createUserSchema, updateUserSchema };
