const Joi = require('@hapi/joi');
Joi.objectId = require('joi-objectid')(Joi);

const schema = Joi.object().keys({
  id:Joi.objectId().required(),
  dateStart: Joi.date().default(new Date()).required(),
  dateEnd: Joi.date().default(new Date()).required(),
  parkingId: Joi.string(),
});

module.exports = schema;
